import Button from "@material-ui/core/Button";
import React from "react";
import "./Navigator.css";

interface NavigatorProps {
  page: number;
  setPage: React.Dispatch<React.SetStateAction<number>>;
  totalPages: number;
}

function Navigator({ page, setPage, totalPages }: NavigatorProps) {
  return (
    <div className="Navigator-body">
      {page > 1 ? (
        <Button onClick={(_) => setPage(page - 1)} variant="contained">
          Prev
        </Button>
      ) : null}

      <span className="Navigator-page-number">{page}</span>

      {page < totalPages ? (
        <Button onClick={(_) => setPage(page + 1)} variant="contained">
          Next
        </Button>
      ) : null}
    </div>
  );
}

export default Navigator;
