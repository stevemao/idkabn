import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import "./App.css";
import { unsplash } from "./config";
import urlJoin from "url-join";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import Navigator from "./Navigator";
import { useDebounce } from "use-debounce";

const useStyles = makeStyles((theme) => ({
  search: {
    "& > *": {
      margin: theme.spacing(1),
      width: "25ch",
    },
  },
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper,
    marginTop: "50px",
    marginBottom: "50px",
  },
  gridList: {
    width: 700,
  },
}));

interface Photo {
  id: string;
  width: number;
  height: number;
  description: string;
  urls: {
    raw: string;
    full: string;
    regular: string;
    small: string;
    thumb: string;
  };
}

function App() {
  const classes = useStyles();
  const [tag, setTag] = useState("");
  const [photos, setPhotos] = useState<Photo[]>([]);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const [debouncedTag] = useDebounce(tag, 1000);

  useEffect(() => {
    const params = new URLSearchParams([
      ["query", debouncedTag],
      ["client_id", unsplash.accessKey],
      ["per_page", String(unsplash.perPage)],
      ["page", String(page)],
    ]);

    fetch(
      `${urlJoin(
        unsplash.baseUrl,
        unsplash.searchPhotosPath
      )}?${params.toString()}`,
      {
        method: "GET",
      }
    )
      .then((res) => res.json())
      .then((data) => {
        setPhotos(data.results);
        setTotalPages(data.total_pages);
      })
      .catch((error) => console.log(error));
  }, [debouncedTag, page]);

  return (
    <div className="App">
      <form className={classes.search} noValidate autoComplete="off">
        <TextField
          label="Search photos"
          value={tag}
          onChange={(e) => setTag(e.target.value)}
        />
      </form>

      <div className={classes.root}>
        <GridList cellHeight={160} className={classes.gridList}>
          {photos.map((photo: Photo) => (
            <GridListTile key={photo.id} cols={photo.width / 10000}>
              <img src={photo.urls.regular} alt={photo.description} />
            </GridListTile>
          ))}
        </GridList>
      </div>

      {photos.length > 0 ? (
        <Navigator page={page} setPage={setPage} totalPages={totalPages} />
      ) : null}
    </div>
  );
}

export default App;
